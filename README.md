# Pipeline Duration Checker

Short bash script that checks the duration of the last 10 pipelines run.

# Use

To use, set the variables TOKEN and PROJECT_ID in-line when running the script. TOKEN should be your Personal Access Token and PROJECT_ID should be the relevant project id.

# Notes

The intention was to demonstrate usage for the [API Module](https://gitlab.com/gitlab-com/support/support-training/-/issues/1952). [GraphQL](https://docs.gitlab.com/ee/api/graphql/reference/) may ultimately be more efficient. We are looking at creating documentation for CI Minutes usage.
