#!/bin/bash

##
# The intention of this script is to check the duration of pipelines for a given project. WORK IN PROGRESS
##
##
# To use this script, pass in your PAT via env var TOKEN and your project ID via PROJECT_ID in-line
##

# TOKEN=""
# PROJECT_ID=""
DATE=$(date +"%m-%Y")

# Gets the project name
function get_project_name() {
	echo ""
	curl -s --header "PRIVATE-TOKEN: $TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID" | jq | grep -w "name_with_namespace" | cut -d ":" -f 2 | sed 's/,$//'
}

# Gets the last 10 pipelines from the project
function get_pipelines() {
	echo ""
	echo "Getting pipelines from project..."
	curl -s --header "PRIVATE-TOKEN: $TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/pipelines?per_page=10" | jq '.' > pipeline_data_$DATE.txt 
	echo ""
}

# Gets all the Pipeline IDs from the last 10 pipelines
function get_pipeline_id() {
	echo ""
	echo "Getting a list of pipeline IDs..."
	cat pipeline_data_*.txt | grep -w "id" | cut -d ":" -f 2 | sed 's/,$//' > pipeline_id_list.txt
	echo ""	
}

# Grabs the duration of each pipeline and stores this in a file
function get_duration() {
	echo ""
	echo "Getting duration of last 10 pipelines..."
	declare -a pipelines_in_seconds
	cat pipeline_id_list.txt | while read PIPELINE_ID
		do
   			curl -s --header "PRIVATE-TOKEN: $TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/pipelines/$PIPELINE_ID" | jq | grep -w "duration" | cut -d ":" -f 2 | sed 's/,$//' >> pipeline_seconds_list.txt
		done
	echo ""	
}

# Convert pipeline duration to a human readable format
function displaytime() {
  local T=$1
  local D=$((T/60/60/24))
  local H=$((T/60/60%24))
  local M=$((T/60%60))
  local S=$((T%60))
  (( $D > 0 )) && printf '%d days ' $D
  (( $H > 0 )) && printf '%d hours ' $H
  (( $M > 0 )) && printf '%d minutes ' $M
  (( $D > 0 || $H > 0 || $M > 0 )) && printf 'and '
  printf '%d seconds\n' $S
}

get_project_name
get_pipelines
get_pipeline_id
get_duration
displaytime

# Read each line of the pipeline_seconds_list file and display the duration in a friendly format
while IFS= read -r line; 
do
	displaytime "$line"
done <pipeline_seconds_list.txt

# Remove all the files that were generated
rm pipeline_data_*.txt pipeline_id_list.txt pipeline_seconds_list.txt

